package pooling;

import javax.management.MBeanServerConnection;

/**
 * @author Ganesh
 * @date 07-04-2015
 * @since 1.0-SNAPSHOT
 */
public interface Work {

    public void doWork(MBeanServerConnection serverConnection) throws Exception;
}
