package utils;

import model.RowSpecForBean;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static utils.RowSpecUtils.rowEntryForAll;

/**
 * @author Ganesh
 * @date 07-04-2015
 * @since 1.0-SNAPSHOT
 */
public class ExcelUtilForHibStats {

    public static final String HEADER = "HEADER";

    public static void exportQueryStatsToExcel(String fileName, Map<String, Object>... queryMap) throws IOException {
        Workbook wb = new XSSFWorkbook();
        for (Map<String, Object> obj : queryMap) {
            obj.remove(OperationalUtil.OP_CODES_LIST);
            obj.remove(OperationalUtil.OBJECT_NAME);
            if (obj.size() == 1) continue;
            Class header = (Class) obj.get(HEADER);
            if (RowSpecUtils.getCache().get(header) == null) continue;
            Sheet qSheet = createQueriesSheet(wb, header.getSimpleName(), header);
            enterRowForValues(obj, qSheet, header);
        }
        FileOutputStream out = new FileOutputStream(fileName);
        wb.write(out);
        out.close();
    }

    @SuppressWarnings("SameParameterValue")
    public static void importQueryStatsFromExcel(String fileName, boolean classpath, List<Map<String, Object>> mapList) throws IOException, InvalidFormatException, ClassNotFoundException {
        InputStream inp = classpath ? ExcelUtilForHibStats.class.getResourceAsStream(fileName) : new FileInputStream("sampleSheet.xlsx");
        Workbook wb = WorkbookFactory.create(inp);
        int numberOfSheets = wb.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheetAt = wb.getSheetAt(i);
            String className = ((XSSFWorkbook) wb).getProperties().getCustomProperties().getProperty(sheetAt.getSheetName()).getLpwstr();
            int firstRowNum = sheetAt.getFirstRowNum();
            int lastRowNum = sheetAt.getLastRowNum();
            Class<?> header = Class.forName(className);
            RowSpecForBean bean = RowSpecUtils.getCache().get(header);
            Map<String, Object> sheetData = new ConcurrentHashMap();
            for (int k = firstRowNum; k <= lastRowNum; k++) {
                if (k == firstRowNum) continue;
                Map<String, Object> rowValue = new HashMap<String, Object>();
                Row row = sheetAt.getRow(k);
                bean.readRow(row, rowValue);
                sheetData.put(rowValue.get(bean.getBeanHeaderName()).toString(), rowValue);
                sheetData.put(ExcelUtilForHibStats.HEADER, header);
            }
            mapList.add(sheetData);
        }

    }

    private static void enterRowForValues(Map<String, Object> queryMap, Sheet qSheet, Class<?> header) {
        int i = 0;
        for (String key : queryMap.keySet()) {
            if (key.equals(HEADER)) continue;
            i++;
            //noinspection unchecked
            rowEntryForAll(qSheet, i, (Map<String, Object>) queryMap.get(key), header);
        }
    }

    private static Sheet createQueriesSheet(Workbook wb, String sheetName, Class<?> classType) {
        Sheet sheet = wb.createSheet(sheetName);
        Row titleRow = sheet.createRow(0);
        RowSpecUtils.getCache().get(classType).writeHeader(titleRow, sheet);
        POIXMLProperties.CustomProperties customProperties = ((XSSFWorkbook) wb).getProperties().getCustomProperties();
        customProperties.addProperty(sheetName, classType.getName());
        return sheet;
    }

    public static String translateMapOfCompositeData() {


        return "";
    }

    public static void setCellComment(Cell cell, String message) {
        Drawing drawing = cell.getSheet().createDrawingPatriarch();
        CreationHelper factory = cell.getSheet().getWorkbook()
                .getCreationHelper();
        // When the comment box is visible, have it show in a 1x3 space
        ClientAnchor anchor = factory.createClientAnchor();
        anchor.setCol1(cell.getColumnIndex());
        int offset = 2;
        anchor.setCol2(cell.getColumnIndex() + offset);
        anchor.setRow1(cell.getRowIndex());
        anchor.setRow2(cell.getRowIndex() + offset);
        anchor.setDx1(100);
        anchor.setDx2(100);
        anchor.setDy1(100);
        anchor.setDy2(100);

        // Create the comment and set the text+author
        Comment comment = drawing.createCellComment(anchor);
        RichTextString str = factory.createRichTextString(message);
        comment.setString(str);
        comment.setAuthor("Apache POI");

        // Assign the comment to the cell
        cell.setCellComment(comment);
    }
}
