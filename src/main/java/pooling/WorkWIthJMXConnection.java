package pooling;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author Ganesh
 * @date 07-04-2015
 * @since 1.0-SNAPSHOT
 */
public class WorkWIthJMXConnection {

    /**
     * Timeout in seconds
     */
    public static int TIMEOUT = 100;
    public static int CAPACITY = 15;
    private static final BlockingQueue<MBeanServerConnection> connections = new ArrayBlockingQueue<MBeanServerConnection>(CAPACITY);
    public static boolean blocking = true;

    /**
     * This will initialize the JMX connection pool with given servieURL and creds passed on by environment
     * In case of error we only spit onto system error
     * available @version 1.0
     *
     * @param serviceURL  JMX service url for the connection
     * @param environment environment map containing all details
     * @throws Exception Exception thrown when connection fails
     */
    public static void initialize(final String serviceURL, final Map<String, ?> environment) throws Exception {
        List<Future<Boolean>> futureList = new LinkedList<Future<Boolean>>();
        for (int i = 0; i < CAPACITY; i++) {
            Future<Boolean> future;

            //noinspection unchecked
            future = GenericDaemonExecutors.submitFuture(new Callable() {
                @Override
                public Object call() throws Exception {
                    JMXServiceURL url = new JMXServiceURL(serviceURL);
                    JMXConnector jmxc = JMXConnectorFactory.connect(url, environment);
                    try {
                        MBeanServerConnection mBeanServerConnection = jmxc.getMBeanServerConnection();
                        connections.put(mBeanServerConnection);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        System.exit(1);

                    }

                    return true;
                }
            });
            futureList.add(future);
        }


        if (WorkWIthJMXConnection.blocking) {
            for (Future fut : futureList) {
                try {
                    fut.get(TIMEOUT, TimeUnit.SECONDS);
                } catch (Exception e) {
                    System.err.println("Connection Exception!");
                    e.printStackTrace();
                    throw e;
                }
            }
        }
    }

    /**
     * This is presently the only way to interact using connections.
     *
     * @param work Should be an implemenation that makes good use of the connection
     * @throws Exception
     */
    public static void workOnConnection(Work work) throws Exception {
        MBeanServerConnection taken = connections.take();
        work.doWork(taken);
        connections.put(taken);
    }


}
