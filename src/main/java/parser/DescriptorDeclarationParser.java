package parser;

import model.DescriptorSpecForBean;
import model.DescriptorType;
import utils.RowSpecUtils;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ganesh on 07-04-2015.
 */
public class DescriptorDeclarationParser implements PatternParser {
    private final Pattern pattern = Pattern.compile("(.*)(.class_descriptor)");

    @Override
    public void onPattern(String key, String value) throws ClassNotFoundException {

        Matcher matcher = pattern.matcher(key);
        if (matcher.matches()) {
            String className = matcher.group(1);
            Class<?> key1 = Class.forName(className);
            DescriptorSpecForBean descriptorSpecForBean;
            if (value.equals("beanDescriptor"))
                descriptorSpecForBean = RowSpecUtils.getDescriptorCache().get(key1) == null ? new DescriptorSpecForBean(null, DescriptorType.BeanDescriptor) : RowSpecUtils.getDescriptorCache().get(key1);
            else if (value.equals("listDescriptor"))
                descriptorSpecForBean = RowSpecUtils.getDescriptorCache().get(key1) == null ? new DescriptorSpecForBean(new HashMap<String, String>(), DescriptorType.ListDescriptor) : RowSpecUtils.getDescriptorCache().get(key1);
            else throw new UnsupportedOperationException("Only List Desriptor and/or Bean Descriptor are supported ");
            RowSpecUtils.getDescriptorCache().put(key1, descriptorSpecForBean);
        }
    }

    @Override
    public boolean matches(String key) {
        Matcher matcher = pattern.matcher(key);
        return matcher.matches();
    }
}
