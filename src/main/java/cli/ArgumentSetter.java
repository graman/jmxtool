package cli;

import main.JMXCrawler;
import org.apache.commons.cli.*;
import pooling.WorkWIthJMXConnection;
import utils.OperationalUtil;

import javax.management.remote.JMXConnector;
import java.util.HashMap;

/**
 * @author Ganesh
 * @date 08-04-2015.
 * @since 1.0-SNAPSHOT
 */
@SuppressWarnings("AccessStaticViaInstance")
public class ArgumentSetter {
    private static CommandLine cmd = null;

    public static String prepareCommandLineOptions(String[] args, HashMap<String, Object> env) throws ParseException {
        cmd = parseCommandlineOptions(args);
        String serviceURL = cmd.getOptionValue("serviceURL");//"service:jmx:remoting-jmx://testapp2:9999";//"service:jmx:rmi:///jndi/rmi://testapp2:9999/jmxrmi";
        String creds[] = new String[2];
        creds[0] = cmd.getOptionValue("user");
        creds[1] = cmd.getOptionValue("password");
        env.put(JMXConnector.CREDENTIALS, creds);
        String optionValue = parseCommandlineOptions(args).hasOption("poolSize") ? parseCommandlineOptions(args).getOptionValue("poolSize") : "15";
        int poolSize = null != optionValue ? Integer.parseInt(optionValue) : 15;
        WorkWIthJMXConnection.CAPACITY = poolSize > 0 ? poolSize : 15;
        WorkWIthJMXConnection.blocking = !parseCommandlineOptions(args).hasOption("blocking");
        JMXCrawler.fileName = cmd.hasOption("OutputFileName") ? cmd.getOptionValue("OutputFileName") : "queryStats.xlsx";
        WorkWIthJMXConnection.TIMEOUT = cmd.hasOption("waitTO") ? Integer.parseInt(cmd.getOptionValue("waitTO")) : 100;
        OperationalUtil.USE_FORK_JOIN = cmd.hasOption("fj");
        return serviceURL;
    }

    public static CommandLine parseCommandlineOptions(String[] args) throws ParseException {
        if (null != cmd) return cmd;
        Options options = new Options();
        Option help = new Option("help", "print help/usage information");
        Option jmxServiceUrl = OptionBuilder.withArgName("url").hasArg().withDescription("use given JMX Service URL").create("serviceURL");
        Option jmxUserName = OptionBuilder.withArgName("user-name").hasArg().withDescription("user-name").create("user");
        Option jmxPassword = OptionBuilder.withArgName("password").hasArg().withDescription("password").create("password");
        Option hibernateStatsObjName = OptionBuilder.withArgName("stats-object-name").hasArg().withDescription("hibernate-stats-object-name").create("hibernateStatsObjectName");
        Option blocking = OptionBuilder.withDescription("for fast-path progress").create("blocking");
        Option poolSize = OptionBuilder.withArgName("poolSize").hasArg().withDescription("jmx pool size setting").create("poolSize");
        Option fileName = OptionBuilder.withArgName("OutputFileName").hasArg().withDescription("Output File Name").create("OutputFileName");
        Option forkJoin = OptionBuilder.withDescription("if enabled will use fork-join intead of executor-service to gather stats").create("fj");
        Option poolingTTL = OptionBuilder.withArgName("pool-TO").hasArg().withDescription("seconds until which system waits for initial JMX connection to be established else returns failure").create("waitTO");
        options.addOption(help).addOption(jmxServiceUrl).addOption(jmxUserName).addOption(jmxPassword).addOption(hibernateStatsObjName).addOption(blocking).addOption(poolSize).addOption(fileName).addOption(forkJoin).addOption(poolingTTL);

        CommandLineParser parser = new BasicParser();
        parser.parse(options, args);
        CommandLine cmd = parser.parse(options, args);
        if (!cmd.hasOption("serviceURL")) {

            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(args[0], options);
            throw new IllegalStateException("serviceURL parameter is needed to initiate JXM connections");
        }
        return cmd;
    }
}
