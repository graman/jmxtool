package model;

/**
 * @author Ganesh
 * @date 27-04-2015
 * @since 1.0-SNAPSHOT
 */
public enum DescribeType {
    NORMAL, DESCRIBE;
}
