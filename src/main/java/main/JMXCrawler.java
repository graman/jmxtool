package main;

import model.DescribeType;
import model.InvocationType;
import model.OpCode;
import org.hibernate.stat.CollectionStatistics;
import org.hibernate.stat.EntityStatistics;
import org.hibernate.stat.QueryStatistics;
import pooling.GenericExecutors;
import utils.ExcelUtilForHibStats;
import utils.OperationalUtil;
import utils.RowSpecUtils;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static cli.ArgumentSetter.parseCommandlineOptions;
import static cli.ArgumentSetter.prepareCommandLineOptions;
import static pooling.WorkWIthJMXConnection.initialize;
import static utils.ExcelUtilForHibStats.exportQueryStatsToExcel;
import static utils.OperationalUtil.collectionJobSubmit;

/**
 * @author Ganesh
 * @date 09-02-2015
 * @since 1.0-SNAPSHOT
 */
public class JMXCrawler {


    public static final long COLLECTOR_SERVICE_SLEEP_INTERVAL = 1000l;
    public static String fileName;

    public static void main(String[] args) throws Exception {
        final HashMap<String, Object> env = new HashMap<String, Object>();
        String serviceURL = prepareCommandLineOptions(args, env);

        RowSpecUtils.init();

        try {
            initialize(serviceURL, env);
        } catch (Exception ex) {
            System.err.println("Error connecting to given server:: " + serviceURL);
            ex.printStackTrace();
            System.exit(1);
        }
        String hibernateStatsObjectName = parseCommandlineOptions(args).hasOption("hibernateStatsObjectName") ? parseCommandlineOptions(args).getOptionValue("hibernateStatsObjectName") : "Hibernate:application=Statistics";

        final ConcurrentMap<String, Object> queryMap = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> entityStatisticsMap = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> collectionStatisticsMap = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> threadDetails = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> memDetails = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> oSDetails = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> runtimeDetails = new ConcurrentHashMap<String, Object>();

        ArrayList<OpCode> queryCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(queryCodes, "Queries", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", String[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(queryCodes, "getQueryStatistics", OpCode.METHOD_INVOKE, new String[]{String.class.getName()}, InvocationType.ASYNC, "Queries", String[].class, DescribeType.DESCRIBE);
        prepareMapForBean(hibernateStatsObjectName, queryMap, QueryStatistics.class, queryCodes);

        ArrayList<OpCode> entityCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(entityCodes, "EntityNames", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", String[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(entityCodes, "getEntityStatistics", OpCode.METHOD_INVOKE, new String[]{String.class.getName()}, InvocationType.ASYNC, "EntityNames", String[].class, DescribeType.DESCRIBE);
        prepareMapForBean(hibernateStatsObjectName, entityStatisticsMap, EntityStatistics.class, entityCodes);

        ArrayList<OpCode> collCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(collCodes, "CollectionRoleNames", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", String[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(collCodes, "getCollectionStatistics", OpCode.METHOD_INVOKE, new String[]{String.class.getName()}, InvocationType.ASYNC, "CollectionRoleNames", String[].class, DescribeType.DESCRIBE);

        prepareMapForBean(hibernateStatsObjectName, collectionStatisticsMap, CollectionStatistics.class, collCodes);

        ArrayList<OpCode> threadCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(threadCodes, "AllThreadIds", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", long[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(threadCodes, "getThreadInfo", OpCode.METHOD_INVOKE, new String[]{long.class.getName()}, InvocationType.ASYNC, "AllThreadIds", long[].class, DescribeType.DESCRIBE);


        prepareMapForBean("java.lang:type=Threading", threadDetails, ThreadMXBean.class, threadCodes);


        ArrayList<OpCode> memCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(memCodes, "HeapMemoryUsage", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(memCodes, "NonHeapMemoryUsage", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareMapForBean("java.lang:type=Memory", memDetails, MemoryMXBean.class, memCodes);


        ArrayList<OpCode> osCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(osCodes, "FreePhysicalMemorySize", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "FreeSwapSpaceSize", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "TotalPhysicalMemorySize", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "TotalSwapSpaceSize", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "ProcessCpuTime", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "CommittedVirtualMemorySize", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "Name", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "Version", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "Arch", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "AvailableProcessors", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(osCodes, "SystemLoadAverage", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareMapForBean("java.lang:type=OperatingSystem", oSDetails, OperatingSystemMXBean.class, osCodes);


        ArrayList<OpCode> runtimeCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(runtimeCodes, "Name", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "ClassPath", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "StartTime", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "SystemProperties", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "VmVersion", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "VmName", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "VmVendor", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "BootClassPath", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "LibraryPath", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "BootClassPathSupported", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "InputArguments", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "ManagementSpecVersion", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "SpecName", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "SpecVendor", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "SpecVersion", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareOpCodesForItems(runtimeCodes, "Uptime", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", javax.management.openmbean.CompositeData.class, DescribeType.DESCRIBE);
        prepareMapForBean("java.lang:type=Runtime", runtimeDetails, RuntimeMXBean.class, runtimeCodes);


        collectionJobSubmit(queryMap, entityStatisticsMap, collectionStatisticsMap, threadDetails, memDetails, oSDetails, runtimeDetails);


        System.out.println("Writing Query stats to excel " + System.currentTimeMillis());
        GenericExecutors.shutdown();
        //noinspection unchecked
        exportQueryStatsToExcel(fileName, queryMap, entityStatisticsMap, collectionStatisticsMap, threadDetails, memDetails, oSDetails, runtimeDetails);
        System.out.println("Finished !! " + System.currentTimeMillis());

    }

    /**
     * Prepares OpCodes for corresponding items
     *
     * @param queryCodes    the list of OpCodes which will be populated with corresponding op-code
     * @param name          name of each operation , with which its output is identifiable as input for other Ops
     * @param operation     Kind of operation involved
     * @param signature     Method signature for RMI method invokes
     * @param async         presently unused as all Operations within one list of Codes are linearly sync.
     * @param input         Name of another Op whose result will be used as input in this
     * @param attributeType Class type of attribute expected
     * @param describe      If so , it will try and persist results using OGL descriptor or plain bean type ones.
     * @return
     */
    public static List<OpCode> prepareOpCodesForItems(List<OpCode> queryCodes, String name, String operation, String[] signature, InvocationType async, String input, Class attributeType, DescribeType describe) {
        {
            queryCodes.add(new OpCode(name, operation, signature, async == InvocationType.ASYNC, input, attributeType, describe == DescribeType.DESCRIBE));
        }
        return queryCodes;
    }

    /**
     * @param objectName
     * @param queryMap
     * @param headerClass
     * @throws MalformedObjectNameException
     */
    public static void prepareMapForBean(String objectName, ConcurrentMap<String, Object> queryMap, Class<?> headerClass, List<OpCode> codes) throws MalformedObjectNameException {
        queryMap.put(OperationalUtil.OBJECT_NAME, new ObjectName(objectName));
        queryMap.put(ExcelUtilForHibStats.HEADER, headerClass);
        queryMap.put(OperationalUtil.OP_CODES_LIST, codes);
    }


}
