package utils;

import model.DescriptorSpecForBean;
import model.RowSpecForBean;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import parser.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;

/**
 * @author Ganesh
 * @date 06-04-2015
 * @since 1.0-SNAPSHOT
 */
public class RowSpecUtils {

    private static final String CORE_POI_PROPERTIES = "/core.poi.properties";
    private static final String POI_CONFIG_DIR = "poi.config.dir";
    private static final Map<Class, RowSpecForBean> cache = new HashMap<Class, RowSpecForBean>();
    private static final List<PatternParser> patternParserList = new LinkedList<PatternParser>();
    private static final Map<Class, DescriptorSpecForBean> descriptorCache = new HashMap<Class, DescriptorSpecForBean>();

    public static void init() {

        addParser();
        initCache();
    }

    private static void addParser() {
        patternParserList.add(new FilterPatternParser());
        patternParserList.add(new ColSizePatternParser());
        patternParserList.add(new DescriptorDeclarationParser());
        patternParserList.add(new DescriptorListDefinitionParser());
    }

    public static Map<Class, DescriptorSpecForBean> getDescriptorCache() {
        return descriptorCache;
    }

    private static void initCache() {
        String property = System.getProperty(POI_CONFIG_DIR);

        try {
            loadCoreProperties();
            if (null != property && (!property.isEmpty())) {
                loadOverrideProperties(property);

            }
        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                e.printStackTrace();
                System.exit(1);
            }
        }


    }

    private static void loadOverrideProperties(String property) throws Exception {
        File configLocation = new File(property);
        File[] files = getFilteredFiles(configLocation);
        Properties properties = new Properties();

        if (null != files) {
            for (File file : files) {
                loadFromFile(properties, file);
            }
        }
    }

    private static void loadCoreProperties() throws Exception {
        Properties coreProperties = new Properties();
        try {
            coreProperties.load(RowSpecUtils.class.getResourceAsStream(CORE_POI_PROPERTIES));
            loadFromProperty(coreProperties);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static File[] getFilteredFiles(File configLocation) {
        return configLocation.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {

                return name.endsWith(".properties");
            }
        });
    }

    private static void loadFromFile(Properties properties, File file) throws Exception {
        FileInputStream inStream;
        try {
            properties.clear();
            inStream = new FileInputStream(file);
            properties.load(inStream);
            loadFromProperty(properties);
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void loadFromProperty(Properties properties) throws Exception {
        for (String key : properties.stringPropertyNames()) {

            for (PatternParser parser : patternParserList) {
                if (parser.matches(key)) {
                    parser.onPattern(key, properties.getProperty(key));
                }
            }
        }
    }

    public static void rowEntryForAll(Sheet sheet, int i, Map<String, Object> stats, Class<?> header) {
        Row queryROw = sheet.createRow(i);

        cache.get(header).writeRow(queryROw, stats, sheet);

    }

    public static Map<Class, RowSpecForBean> getCache() {
        return cache;
    }

}
