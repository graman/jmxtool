package model;

/**
 * @author Ganesh
 * @date 16-04-2015
 * @since 1.0-SNAPSHOT
 */
public class OpCode {

    public static final String ATTRIBUTE_GET = "GET_ATTRIBUTE";
    public static final String METHOD_INVOKE = "INVOKE_METHOD";
    public static final String METHOD_NAME = "METHOD_NAME";
    public static final String METHOD_SIGNATURE = "METHOD_SIGNATURE";
    public static final String DESCRIBE_MAP = "DESCRIBE_MAP";

    private String operation;
    private String name;
    private String[] signature;
    private boolean async = false;
    private String input;
    private Object result;
    private Class attributeType;
    private boolean describe;

    /**
     * Constructs an OpCode with
     *
     * @param name          denoting the name of the operation
     * @param operation     denoting either a Attribute get or method Invoke
     * @param parameters    used to specify input param type for method invoke interface of JMX RMI
     * @param async         TODO introduce API changes to make OPCode async hint participate in dependency decisions
     * @param input         indicates which OpCode's output will be used as input for this execution by name and used in dependency decision
     * @param attributeType type of attribute expected as output
     * @param describe      if set, OGNL descriptor/default descriptor will be used to describe the output of this OpCode .This influences dependency decision as well.
     */
    public OpCode(String name, String operation, String[] parameters, boolean async, String input, Class attributeType, boolean describe) {
        this.name = name;
        this.operation = operation;
        this.signature = parameters;
        this.async = async;
        this.input = input;
        this.attributeType = attributeType;
        this.describe = describe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String[] getSignature() {
        return signature;
    }

    public void setSignature(String[] signature) {
        this.signature = signature;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public Class getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(Class attributeType) {
        this.attributeType = attributeType;
    }

    public boolean isDescribe() {
        return describe;
    }

    public void setDescribe(boolean describe) {
        this.describe = describe;
    }
}
