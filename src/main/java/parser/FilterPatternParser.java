package parser;

import model.RowSpecForBean;
import utils.RowSpecUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ganesh on 07-04-2015.
 */
public class FilterPatternParser implements PatternParser {
    private final Pattern pattern = Pattern.compile("(.*)(.row_spec.filters)");

    @Override
    public void onPattern(String key, String value) throws ClassNotFoundException {

        Matcher matcher = pattern.matcher(key);
        if (matcher.matches()) {
            String className = matcher.group(1);
            Class<?> aClass = Class.forName(className);
            RowSpecForBean rowSpecForBean = RowSpecUtils.getCache().get(aClass) == null ? new RowSpecForBean() : RowSpecUtils.getCache().get(aClass);
            rowSpecForBean.setFilteredProperties(value.split(","));
            RowSpecUtils.getCache().put(aClass, rowSpecForBean);
        }
    }

    @Override
    public boolean matches(String key) {
        Matcher matcher = pattern.matcher(key);
        return matcher.matches();
    }
}
