package model;

import ognl.DefaultMemberAccess;
import ognl.OgnlContext;
import ognl.OgnlException;
import org.apache.commons.beanutils.BeanUtils;
import pooling.Work;
import utils.RowSpecUtils;

import javax.management.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

import static ognl.Ognl.getValue;
import static ognl.Ognl.parseExpression;

/**
 * @author Ganesh
 * @date 10-04-2015
 * @since 1.0-SNAPSHOT
 */
public class GatherDataWorker implements Work {
    private final ObjectName objectName;
    private final Object[] ent;
    private final String methodName;
    private final ConcurrentMap<String, Object> entityMap;
    private final boolean isDescribe;
    private final String[] signature;

    public GatherDataWorker(ObjectName objectName, Object[] ent, String methodName, ConcurrentMap<String, Object> entityMap, boolean isDescribe, String[] signature) {
        this.objectName = objectName;
        this.ent = ent;
        this.methodName = methodName;
        this.entityMap = entityMap;
        this.isDescribe = isDescribe;
        this.signature = signature;
    }

    public static Map<String, String> descibeResultAsNameValuePari(Object entityStats) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        DescriptorSpecForBean descriptorSpecForBean = RowSpecUtils.getDescriptorCache().get(entityStats.getClass());
        if (descriptorSpecForBean == null)
            return BeanUtils.describe(entityStats);
        else if (descriptorSpecForBean.getType() == DescriptorType.BeanDescriptor)
            return BeanUtils.describe(entityStats);
        else if (descriptorSpecForBean.getType() == DescriptorType.ListDescriptor) {
            Map<String, String> desc = new HashMap<String, String>();
            DefaultMemberAccess access = new DefaultMemberAccess(true, true, true);
            OgnlContext context = new OgnlContext();
            context.setMemberAccess(access);

            for (String name : descriptorSpecForBean.getFields().keySet()) {
                try {
                    Object value = getValue(parseExpression(descriptorSpecForBean.getFields().get(name)), context, entityStats);
                    desc.put(name, String.valueOf(value));
                } catch (OgnlException e) {
                    e.printStackTrace();
                }
            }
            return desc;
        } else {
            throw new IllegalStateException("It has to be either a BeanDescriptor/ListDescriptor,Support for others is not yet there!");
        }
    }

    @Override
    public void doWork(MBeanServerConnection serverConnection) throws Exception {
        Object output = getCollectionStats(serverConnection, objectName, ent, methodName, signature);
        if (isDescribe) {
            Map<String, Object> values = new HashMap<String, Object>();
            values.putAll(descibeResultAsNameValuePari(output));
            entityMap.put(String.valueOf(ent[0]), values);
        }
    }

    /**
     * Returns a new hash-map value representing the attribute values of the system
     *
     * @param serverConnection
     * @param replication1
     * @param query
     * @param methodName
     * @param signature
     * @return
     * @throws InstanceNotFoundException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    private Object getCollectionStats(MBeanServerConnection serverConnection, ObjectName replication1, Object[] query, String methodName, String[] signature) throws InstanceNotFoundException, MBeanException, ReflectionException, IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Object entityStats = serverConnection.invoke(replication1, methodName, query, signature);
        return entityStats;
    }

    public final ConcurrentMap<String, Object> getEntityMap() {
        return entityMap;
    }

    public final String getMethodName() {
        return methodName;
    }

    public final ObjectName getObjectName() {
        return objectName;
    }
}
