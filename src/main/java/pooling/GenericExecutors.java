package pooling;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Generic(as against DaemonExecutors) executor wrapper with fixed thread pooling.
 * @author Ganesh
 * @date 07-04-2015
 * @since 1.0-SNAPSHOT
 */
public class GenericExecutors {

    public static int MAX_GENERIC_THREADS = 20;
    private static ExecutorService executor = Executors.newFixedThreadPool(MAX_GENERIC_THREADS);

    public static Future<?> submitTask(Runnable work) {
        return executor.submit(work);
    }

    public static void reinitialize() {
        executor = Executors.newFixedThreadPool(MAX_GENERIC_THREADS);
    }

    public static void shutdown() {
        executor.shutdown();
        executor.isTerminated();
    }
}
