import model.DescribeType;
import model.InvocationType;
import model.OpCode;
import org.hibernate.stat.CollectionStatistics;
import org.hibernate.stat.EntityStatistics;
import org.hibernate.stat.QueryStatistics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pooling.GenericExecutors;
import pooling.Work;
import pooling.WorkWIthJMXConnection;
import utils.ExcelUtilForHibStats;
import utils.RowSpecUtils;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static main.JMXCrawler.prepareMapForBean;
import static main.JMXCrawler.prepareOpCodesForItems;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static utils.OperationalUtil.USE_FORK_JOIN;
import static utils.OperationalUtil.collectionJobSubmit;

/**
 * @author Ganesh
 * @date 09-04-2015
 * @since 1.0-SNAPSHOT
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(WorkWIthJMXConnection.class)
public class TestJMXWIthHiberStats {

    /**
     * An integration test that builds stats model from excel in-memory and then tries to work through the API to unbuild it and compare.
     * This does not include a JMX connection but have stubbed with mocks for brevity.
     * This works up via JDK1.7  fork-join support
     *
     * @throws Exception
     */
/*    @Test(expected = java.lang.AssertionError.class)
    public void testJDK1$7FJ() throws Exception {

        testCollectionStats(true);
    }*/

    /**
     * An integration test that builds stats model from excel in-memory and then tries to work through the API to unbuild it and compare.
     * This does not include a JMX connection but have stubbed with mocks for brevity.
     * This works up via executor service support
     *
     * @throws Exception
     */

    @Test
    public void testViaExecutorServiceJD1$6() throws Exception {

        testCollectionStats(false);
    }


    public void testCollectionStats(boolean forkJoin) throws Exception {

        RowSpecUtils.init();
        WorkWIthJMXConnection.CAPACITY = 2;
        GenericExecutors.MAX_GENERIC_THREADS = 1;
        USE_FORK_JOIN = forkJoin;
        GenericExecutors.reinitialize();
        List<Map<String, Object>> mapList = new LinkedList<Map<String, Object>>();
        ExcelUtilForHibStats.importQueryStatsFromExcel("/sampleSheet.xlsx", true, mapList);

        final Map<String, Object> colls = mapList.get(2);

        String[] collRoleNames = getCollRoleNameString(colls);
        final Map<String, Object> ents = mapList.get(1);
        String[] entityNames = getCollRoleNameString(ents);
        final Map<String, Object> qrs = mapList.get(0);
        String[] queryNames = getCollRoleNameString(qrs);

        final MBeanServerConnection serverConnection = mock(MBeanServerConnection.class);

        PowerMockito.mockStatic(WorkWIthJMXConnection.class);

        PowerMockito.when(WorkWIthJMXConnection.class, "workOnConnection", any(Work.class)).then(new Answer<Object>() {
                                                                                                     @Override
                                                                                                     public Object answer(InvocationOnMock invocationOnMock) throws Throwable {

                                                                                                         Object[] arguments = invocationOnMock.getArguments();
                                                                                                         Work argument = (Work) arguments[0];
                                                                                                         argument.doWork(serverConnection);
                                                                                                         return null;
                                                                                                     }
                                                                                                 }
        );


        when(serverConnection.getAttribute(any(ObjectName.class), eq("CollectionRoleNames"))).thenReturn(collRoleNames);
        when(serverConnection.getAttribute(any(ObjectName.class), eq("EntityNames"))).thenReturn(entityNames);
        when(serverConnection.getAttribute(any(ObjectName.class), eq("Queries"))).thenReturn(queryNames);

        when(serverConnection.invoke(any(ObjectName.class), eq("getCollectionStatistics"), any(String[].class), any(String[].class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                Object[] query = (Object[]) arguments[2];
                Object qry = query[0];
                return colls.get(qry);
            }
        });
        when(serverConnection.invoke(any(ObjectName.class), eq("getEntityStatistics"), any(String[].class), any(String[].class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                Object[] query = (Object[]) arguments[2];
                Object qry = query[0];
                return ents.get(qry);
            }
        });
        when(serverConnection.invoke(any(ObjectName.class), eq("getQueryStatistics"), any(String[].class), any(String[].class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                Object[] query = (Object[]) arguments[2];
                Object qry = query[0];
                return qrs.get(qry);
            }
        });


        final ConcurrentMap<String, Object> queryMap = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> entityMap = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> collMap = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> threadDetails = new ConcurrentHashMap<String, Object>();
        final ConcurrentMap<String, Object> memDetails = new ConcurrentHashMap<String, Object>();

        ArrayList<OpCode> queryCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(queryCodes, "Queries", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", String[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(queryCodes, "getQueryStatistics", OpCode.METHOD_INVOKE, new String[]{String.class.getName()}, InvocationType.ASYNC, "Queries", String[].class, DescribeType.DESCRIBE);
        prepareMapForBean("Hibernate:application=Statistics", queryMap, QueryStatistics.class, queryCodes);

        ArrayList<OpCode> entityCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(entityCodes, "EntityNames", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", String[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(entityCodes, "getEntityStatistics", OpCode.METHOD_INVOKE, new String[]{String.class.getName()}, InvocationType.ASYNC, "EntityNames", String[].class, DescribeType.DESCRIBE);
        prepareMapForBean("Hibernate:application=Statistics", entityMap, EntityStatistics.class, entityCodes);

        ArrayList<OpCode> collCodes = new ArrayList<OpCode>();
        prepareOpCodesForItems(collCodes, "CollectionRoleNames", OpCode.ATTRIBUTE_GET, null, InvocationType.SYNC, "none", String[].class, DescribeType.NORMAL);
        prepareOpCodesForItems(collCodes, "getCollectionStatistics", OpCode.METHOD_INVOKE, new String[]{String.class.getName()}, InvocationType.ASYNC, "CollectionRoleNames", String[].class, DescribeType.DESCRIBE);

        prepareMapForBean("Hibernate:application=Statistics", collMap, CollectionStatistics.class, collCodes);


        collectionJobSubmit(queryMap, entityMap, collMap);

        assertEquals(ents.size(), entityMap.size());
        assertEquals(collMap.size(), colls.size());
        assertEquals(queryMap.size(), qrs.size());

        GenericExecutors.shutdown();
    }

    private String[] getCollRoleNameString(Map<String, Object> colls) {
        String[] collRoleNames = new String[colls.keySet().size() - 1];
        int ix = 0;
        for (String str : colls.keySet()) {
            if (str.equals(ExcelUtilForHibStats.HEADER)) continue;
            collRoleNames[ix++] = str;
        }
        return collRoleNames;
    }

}
