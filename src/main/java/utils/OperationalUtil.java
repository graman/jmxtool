package utils;

import main.JMXCrawler;
import model.GatherDataWorker;
import model.OpCode;
import pooling.GenericExecutors;
import pooling.Work;

import javax.management.*;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static java.lang.Thread.sleep;
import static model.GatherDataWorker.descibeResultAsNameValuePari;
import static pooling.WorkWIthJMXConnection.workOnConnection;

/**
 * @author Ganesh
 * @date 06-04-2015
 * @since 1.0-SNAPSHOT
 */
public class OperationalUtil {

    public static final String ATTRIBUTE_NAME_KEY = "ATTRIBUTE_NAME";
    public static final String METHOD_NAME = "METHOD_NAME";
    public static final String CLASS_NAME = "CLASS_NAME";
    public static final String OBJECT_NAME = "OBJECT_NAME";
    public static final String ATTRIBUTE_CLASS = "ATTRIBUTE_CLASS";
    public static final String BEAN_TYPE = "BEAN_TYPE";
    public static final String OP_CODES_LIST = "OP_CODES";
    public static final boolean ASYNC = true;
    public static final boolean SYNC = false;
    public static final String HEADER_TYPE = "HEADER_TYPE";
    public static boolean USE_FORK_JOIN = false;

    /**
     * Used to populate attributes from an MBean
     *
     * @param serverConnection represents the connection usually obtained via @see  pooling.WorkWIthJMXConnection#workOnConnection(pooling.Work work);
     * @param objectName       represents objecName
     * @param entityMap        map of collection/query/entity items for which we wish to collect individual stats
     * @param opCodes
     * @throws MBeanException             thrown due to MBean attribute/method or objecName mis-configurations
     * @throws AttributeNotFoundException attribute name was possibly incorrect?
     * @throws InstanceNotFoundException  instance was not found
     * @throws ReflectionException        error shipping properties from/to stats class
     * @throws IOException                RMI/otherwise
     * @throws ExecutionException         due to executor service being used
     * @throws InterruptedException       due to executor service being used
     * @version 1.2-SNAPSHOT on JDK1.7 changed to  fork-join from executor style.
     * @since 1.0-SNAPSHOT
     */
    private static void getCollectionMap(final MBeanServerConnection serverConnection, final ObjectName objectName, final ConcurrentMap<String, Object> entityMap, List<OpCode> opCodes) throws MBeanException, AttributeNotFoundException, InstanceNotFoundException, ReflectionException, IOException, ExecutionException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        ConcurrentMap<String, Object> opCodeNameOutputMap = new ConcurrentHashMap<String, Object>();
        for (OpCode opCode : opCodes) {
            {
                if (opCode.getOperation().equals(OpCode.ATTRIBUTE_GET)) {
                    String attributeName = opCode.getName();
                    Object entities = getCollections(serverConnection, objectName, attributeName);
                    if (entities != null)
                        opCodeNameOutputMap.put(opCode.getName(), entities);
                    if (opCode.isDescribe()) {
                        Map<String, Object> values = new HashMap<String, Object>();
                        values.putAll(descibeResultAsNameValuePari(entities));
                        values.put("attributeName", attributeName);
                        entityMap.put(String.valueOf(entities), values);
                    }

                } else if (OpCode.METHOD_INVOKE.equals(opCode.getOperation())) {
                    String[] signature = opCode.getSignature();
                    String methodName = opCode.getName();
                    if (opCode.getInput().equals("none")) {
                        invokeMethodOnEntities(objectName, entityMap, methodName, null, null, opCode.isDescribe(), signature);
                    } else {
                        Object entities = opCodeNameOutputMap.get(opCode.getInput());
                        Class attribute = opCode.getAttributeType();
                        invokeMethodOnEntities(objectName, entityMap, methodName, attribute.cast(entities), attribute, opCode.isDescribe(), signature);
                    }
                }
            }
        }
        entityMap.remove(OP_CODES_LIST);
        entityMap.remove(HEADER_TYPE);

    }

    /**
     * Works with a executor service to induce concurrent threads in parallel to process multiple work items
     * The service is configured separatly by init config system
     *
     * @param isDescribe
     * @param signature
     * @param objectName jmx mbean object
     * @param entityMap  a concurrent map that ensures concurrent semantics are enforced
     * @param methodName name of method to activate
     * @param entities   a collection of entities for whose category method would be invoked
     * @param signature  method signature
     * @throws ExecutionException   over executor service failure
     * @throws InterruptedException interrupted over something unexpected
     */
    private static void invokeMethodOnEntities(final ObjectName objectName, final ConcurrentMap<String, Object> entityMap, final String methodName, Object entities, final Class attribute, boolean isDescribe, String[] signature) throws ExecutionException, InterruptedException {
        List<Future> futures = new LinkedList<Future>();

        if (attribute != null && entities != null && attribute.isArray()) {
            {
                for (int i = 0; i < Array.getLength(entities); i++) {
                    Future<?> future = GenericExecutors.submitTask(getRunnable(objectName, entityMap, methodName, Array.get(entities, i), isDescribe, signature));
                    futures.add(future);
                }
            }
            for (Future future : futures) {
                future.get();
            }
        } else {
            Future<?> future = GenericExecutors.submitTask(getRunnable(objectName, entityMap, methodName, entities, isDescribe, signature));
            future.get();

        }
    }

    private static Runnable getRunnable(final ObjectName objectName, final ConcurrentMap<String, Object> entityMap, final String methodName, final Object entity, final boolean isDescribe, final String[] signature) {
        if (entity != null) {
            return new Runnable() {
                @Override
                public void run() {
                    final Object[] ent = new Object[1];
                    ent[0] = entity;
                    try {
                        workOnConnection(new GatherDataWorker(objectName, ent, methodName, entityMap, isDescribe, signature));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };
        } else {
            return new Runnable() {
                @Override
                public void run() {

                    try {
                        workOnConnection(new GatherDataWorker(objectName, null, methodName, entityMap, isDescribe, signature));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };
        }

    }


    private static Object getCollections(MBeanServerConnection serverConnection, ObjectName replication1, String attributeName) throws MBeanException, AttributeNotFoundException, InstanceNotFoundException, ReflectionException, IOException {
        return serverConnection.getAttribute(replication1, attributeName);
    }

    /**
     * @param replication1
     * @param executorService
     * @param collectionStatisticsMap
     * @param opCodes
     */
    private static void submitCollectionStatsJob(final ObjectName replication1, ExecutorService executorService, final ConcurrentMap<String, Object> collectionStatisticsMap, final List<OpCode> opCodes) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    getCollStats(replication1, collectionStatisticsMap, opCodes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Gathers Hibernate collection statstics with iterative calls via JMX connection which is being pooled.
     *
     * @param objectName              repr esents object name for MBean to be fetched
     * @param collectionStatisticsMap a map of all stats items
     * @param opCodes
     * @throws Exception
     */
    private static void getCollStats(final ObjectName objectName, final ConcurrentMap<String, Object> collectionStatisticsMap, final List<OpCode> opCodes) throws Exception {

        Work work = new Work() {
            @Override
            public void doWork(MBeanServerConnection serverConnection) throws Exception {
                getCollectionMap(serverConnection, objectName, collectionStatisticsMap, opCodes);
            }
        };
        workOnConnection(work);

    }

    /**
     * Submits a JMX data gathering job for collections/queries/entites and waits for completion.
     *
     * @param queryMap represents an empty concurrent hash-map upon which fetched query JMX info will be fed
     * @throws InterruptedException
     */
    public static void collectionJobSubmit(ConcurrentMap<String, Object>... queryMap) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        for (ConcurrentMap<String, Object> obj : queryMap) {
            submitCollectionStatsJob((ObjectName) obj.get(OBJECT_NAME), executorService, obj, (List<OpCode>) obj.get(OP_CODES_LIST));
            obj.remove(OBJECT_NAME);
        }
        executorService.shutdown();
        while (true) {

            if (executorService.isTerminated())
                break;
            else {
                sleep(JMXCrawler.COLLECTOR_SERVICE_SLEEP_INTERVAL);
            }
        }
    }

}
