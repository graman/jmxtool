package model;

import java.util.Map;

/**
 * @author Ganesh
 * @date 15-04-2015
 * @since 1.0-SNAPSHOT
 */
public class DescriptorSpecForBean {

    final private Map<String, String> fields;
    final private DescriptorType type;

    public DescriptorSpecForBean(Map<String, String> fields, DescriptorType type) {
        this.fields = fields;
        this.type = type;
    }

    public Map<String, String> getFields() {
        return fields;
    }

    public DescriptorType getType() {
        return type;
    }
}
