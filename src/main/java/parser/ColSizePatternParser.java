package parser;

import model.RowSpecForBean;
import utils.RowSpecUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ganesh on 07-04-2015.
 */
public class ColSizePatternParser implements PatternParser {
    private final Pattern pattern = Pattern.compile("(.*)(.row_spec.col_size)");

    @Override
    public void onPattern(String key, String value) throws Exception {
        Matcher matcher = pattern.matcher(key);
        if (matcher.matches()) {
            String className = matcher.group(1);
            Class<?> aClass = Class.forName(className);
            RowSpecForBean rowSpecForBean = RowSpecUtils.getCache().get(aClass);
            if (rowSpecForBean == null) {
                rowSpecForBean = new RowSpecForBean();

            }
            String[] specs = value.split(",");
            for (String spec : specs) {
                String[] colSize = spec.split("#");
                //noinspection unchecked
                rowSpecForBean.getColWidth().put(colSize[0], Integer.parseInt(colSize[1]));

            }

            RowSpecUtils.getCache().put(aClass, rowSpecForBean);
        }
    }

    @Override
    public boolean matches(String key) {
        Matcher matcher = pattern.matcher(key);
        return matcher.matches();
    }
}
