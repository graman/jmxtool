import cli.ArgumentSetter;
import junit.framework.TestCase;
import model.DescriptorType;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.hibernate.stat.QueryStatistics;
import org.junit.Test;
import pooling.WorkWIthJMXConnection;
import utils.ExcelUtilForHibStats;
import utils.RowSpecUtils;

import javax.management.MBeanServer;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


/**
 * Created by Ganesh on 06-04-2015.
 * This is to test properties parsing,caching and loading
 */
public class TestCoreAttributes {
    /**
     * This is to test core properties are being loaded correctly
     */
    @Test
    public void testCoreProperties() {

        RowSpecUtils.init();
        assertNotNull(RowSpecUtils.getCache().get(QueryStatistics.class));
        assertNotNull(String.valueOf(RowSpecUtils.getCache().get(QueryStatistics.class).getColWidth().size()));
        assertEquals(RowSpecUtils.getCache().size(), 7);
        assertEquals(RowSpecUtils.getCache().get(QueryStatistics.class).getFilteredProperties().size(), 5);
    }


    /**
     * To test if overridden properties are overridden correctly!
     *
     * @throws URISyntaxException
     */
    @Test
    public void testOverriddenProperties() throws URISyntaxException {
        File sampleProps = new File(getClass().getResource("test.properties").toURI());
        System.getProperties().put("poi.config.dir", sampleProps.getParent());
        RowSpecUtils.init();
        assertEquals(RowSpecUtils.getCache().size(), 7);
        assertEquals(RowSpecUtils.getCache().get(QueryStatistics.class).getFilteredProperties().size(), 4);
        assertEquals(RowSpecUtils.getDescriptorCache().size(), 9);
        assertEquals(RowSpecUtils.getDescriptorCache().get(CompositeDataSupport.class).getType(), DescriptorType.ListDescriptor);
        assertEquals(RowSpecUtils.getDescriptorCache().get(CompositeDataSupport.class).getFields().get("threadName"), "contents.threadName");
    }

    /**
     * Sample test for service url
     *
     * @throws Exception
     */
    @Test(expected = IllegalStateException.class)
    public void testMainArgs() throws Exception {

        String[] args = {"JMXCrawler", ""};
        assertFalse(ArgumentSetter.parseCommandlineOptions(args).hasOption("serviceURL"));
    }

    @Test
    public void testConnectionPooling() throws Exception {

        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        java.rmi.registry.LocateRegistry.createRegistry(9000);
        JMXServiceURL serviceURl = new JMXServiceURL(
                "service:jmx:rmi:///jndi/rmi://localhost:9000/server");
        JMXConnectorServer connectorServer = JMXConnectorServerFactory.newJMXConnectorServer(serviceURl, null, platformMBeanServer);
        connectorServer.start();
        assertNotNull(serviceURl);
        Map<String, String> env = new HashMap<String, String>();
        WorkWIthJMXConnection.CAPACITY = 1;
        WorkWIthJMXConnection.blocking = true;
        WorkWIthJMXConnection.initialize(serviceURl.toString(), env);
    }

    @Test
    public void testExcelUtilForHibStats() throws IOException, InvalidFormatException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException, ClassNotFoundException {
        RowSpecUtils.init();
        List<Map<String, Object>> mapList = new LinkedList<Map<String, Object>>();
        ExcelUtilForHibStats.importQueryStatsFromExcel("/sampleSheet.xlsx", true, mapList);
        TestCase.assertEquals(mapList.size(), 3);
    }


}
