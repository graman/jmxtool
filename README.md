# README #

This is a JMX Probe application with very simple objectives

* [x] Record hibernate stats and export it to an excel for :
* [x] Record Query Stats
* [x] Record Collection Stats
* [x] Record Entity Stats


Next set of Goals

* [x] Record thread stats from platform JMX Beans
* [x] Record cpu stats from platform JMX Beans
* [x] Record Memory stats from platform JMX Beans
* [x] Record Misc. OS stats from platform MBean

Future goals:

* [ ] DSLify configuration for custom MBeans configuration and export
* [ ] Integrate for timeseries analytics for low scale data

### What is this repository for? ###

*This repo is currently private as much of API and code is being just formulated iteratively based on amount of free time i have<br/>
*Will release this under OSS License once am convinced of decent start<br/>

### Coverage reports are updated infrequently as of now and will cover most recent major change ###

###The JDK1.7 branch is experimental and master branch uses JDK1.6 and code will be infrequently megered with JDK1.7 branch###

Authod: th3iedkid/Ganesh.Raman