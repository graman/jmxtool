package model;

/**
 * @author Ganesh
 * @date 27-04-2015
 * @since 1.0-SNAPSHOT
 *
 * An invocation type of ASYNC implies
 */
public enum InvocationType {
    ASYNC, SYNC;

}
