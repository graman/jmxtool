package model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.*;

import static utils.ExcelUtilForHibStats.setCellComment;

/**
 * @author Ganesh
 * @date 06-04-2015
 * @since 1.0-SNAPSHOT
 */
public class RowSpecForBean<T> {

    private final Map<String, Integer> colWidth = new HashMap<String, Integer>();
    private List<String> filteredProperties;

    public void writeHeader(Row row, Sheet sheet) {
        int ix = 0;
        for (String key : filteredProperties) {
            String nkey = key.substring(0, 1).toUpperCase() + key.substring(1);
            writeCell(row, ix++, nkey, sheet, key);
        }
    }

    public void writeRow(Row queryROw, Map<String, Object> object, Sheet sheet) {
        if (filteredProperties == null) {
            throw new IllegalStateException("FilterSpec must not be null for RowSpecBean");
        }
        int ix = 0;

        for (String prop : filteredProperties) {

            Object propertyValue = object.get(prop);
            writeCell(queryROw, ix++, propertyValue, sheet, prop);
        }
        int k = 0;
        for (String prop : filteredProperties) {
            setColumnWidthFor(k++, sheet, prop);
        }

    }

    public void readRow(Row queryRow, Map<String, Object> object) {
        int ix = 0;
        for (String prop : filteredProperties) {
            object.put(prop, queryRow.getCell(ix).getStringCellValue());
        }
    }

    public String getBeanHeaderName() {

        return filteredProperties.get(0);

    }


    private void writeCell(Row queryROw, int ix, Object propertyValue, Sheet sheet, String prop) {
        Cell cell = queryROw.createCell(ix);
        String stringValue = getStringValue(propertyValue);
        if (stringValue.length() > 32767) {
            stringValue = stringValue.substring(0, 32765);
            cell.setCellValue(stringValue);
            setCellComment(cell, "text exceeds ms-excel spec limits");
        } else {
            cell.setCellValue(stringValue);
        }
        setColumnWidthFor(ix, sheet, prop);
    }


    private String getStringValue(Object propertyValue) {
        return String.valueOf(propertyValue);
    }

    private void setColumnWidthFor(int ix, Sheet sheet, String prop) {
        Integer width = colWidth.get(prop);
        if (width != null)
            sheet.setColumnWidth(ix, (width) * 256);
    }

    public List<String> getFilteredProperties() {
        return filteredProperties;
    }

    public void setFilteredProperties(String... args) {
        if (null != args) {
            setFilteredProperties(new LinkedList<String>(Arrays.asList(args)));
        }
    }

    void setFilteredProperties(List<String> filteredProperties) {
        this.filteredProperties = filteredProperties;
    }

    public Map<String, Integer> getColWidth() {
        return colWidth;
    }

}
