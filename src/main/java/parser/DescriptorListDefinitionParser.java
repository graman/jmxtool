package parser;

import model.DescriptorSpecForBean;
import model.DescriptorType;
import utils.RowSpecUtils;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ganesh on 07-04-2015.
 */
public class DescriptorListDefinitionParser implements PatternParser {
    private final Pattern pattern = Pattern.compile("(.*)(.list.)(.*)");

    @Override
    public void onPattern(String key, String value) throws ClassNotFoundException {

        Matcher matcher = pattern.matcher(key);
        if (matcher.matches()) {
            String className = matcher.group(1);
            Class<?> key1 = Class.forName(className);
            String attrName = matcher.group(3);
            DescriptorSpecForBean descriptorSpecForBean;
            descriptorSpecForBean = RowSpecUtils.getDescriptorCache().get(key1) != null ? RowSpecUtils.getDescriptorCache().get(key1) : new DescriptorSpecForBean(new HashMap<String, String>(), DescriptorType.ListDescriptor);
            descriptorSpecForBean.getFields().put(attrName, value);
            RowSpecUtils.getDescriptorCache().put(key1, descriptorSpecForBean);
        }
    }

    @Override
    public boolean matches(String key) {
        Matcher matcher = pattern.matcher(key);
        return matcher.matches();
    }
}
