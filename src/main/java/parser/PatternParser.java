package parser;

/**
 * @author Ganesh
 * @date 07-04-2015
 * @since 1.0-SNAPSHOT
 */
public interface PatternParser {

    public void onPattern(String key, String value) throws Exception;

    public boolean matches(String key);

}
