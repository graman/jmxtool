import ognl.DefaultMemberAccess;
import ognl.OgnlContext;
import ognl.OgnlException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.hibernate.stat.QueryStatistics;
import org.junit.Test;
import pooling.GenericExecutors;
import pooling.WorkWIthJMXConnection;
import utils.ExcelUtilForHibStats;
import utils.RowSpecUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static ognl.Ognl.*;
import static org.junit.Assert.assertEquals;
import static utils.OperationalUtil.USE_FORK_JOIN;

/**
 * The test suite will be used to abstract OGNL interaction API for prop extraction as N-V-Pair
 *
 * @author Ganesh
 * @date 15-04-2015
 * @since 1.0-SNAPSHOT
 */

public class TestOGNL {

    public static final String HASH_STRING = "hashString";

    @Test
    public void testOgnlSamples() throws OgnlException {

        DefaultMemberAccess access = new DefaultMemberAccess(true, true, true);
        OgnlContext context = new OgnlContext();
        context.setMemberAccess(access);


        Object hashStringCode = getValuez(context, "hashCode()");
        assertEquals(hashStringCode, HASH_STRING.hashCode());

        Object count = getValuez(context, "count");
        assertEquals(count, HASH_STRING.length());

        assertEquals(getValuez(context, "value.length"), count);


        Runnable runnable = new Runnable() {

            private final HashMap<String, String> aces = new HashMap<String, String>();


            @Override
            public void run() {

            }
        };

        assertEquals(getValue("aces.size()", context, runnable), 0);

        HashMap<String, String> value = new HashMap<String, String>();
        value.put(HASH_STRING, HASH_STRING);
        setValue("aces", context, runnable, value);
        assertEquals(getValue("aces.size()", context, runnable), 1);
        assertEquals(getValue("aces[\"" + HASH_STRING + "\"]", context, runnable), HASH_STRING);
        value.remove(HASH_STRING);
        assertEquals(getValue("aces[\"" + HASH_STRING + "\"]", context, runnable), null);
        Object value1 = getValue("#@java.util.HashMap@{ \"foo\" : \"foo value\", \"bar\" : \"bar value\" }", context, runnable);
        System.out.println(value1.getClass());

    }

    @Test
    public void testRowSpecUtils() throws InvalidFormatException, IOException, ClassNotFoundException {


        RowSpecUtils.init();
        WorkWIthJMXConnection.CAPACITY = 2;
        GenericExecutors.MAX_GENERIC_THREADS = 1;
        USE_FORK_JOIN = false;
        GenericExecutors.reinitialize();
        List<Map<String, Object>> mapList = new LinkedList<Map<String, Object>>();
        ExcelUtilForHibStats.importQueryStatsFromExcel("/sampleSheet.xlsx", true, mapList);

        Map<String, Object> x = mapList.get(0);

        List<String> filteredProperties = RowSpecUtils.getCache().get(QueryStatistics.class).getFilteredProperties();
        String expr = "categoryName,executionCount,executionAvgTime,executionMaxTime,executionMinTime";
        for (String ace : expr.split(",")) {

        }
    }

    private Object getValuez(OgnlContext context, String expression) throws OgnlException {
        return getValue(parseExpression(expression), context, HASH_STRING);
    }
}
