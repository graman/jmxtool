package pooling;

import java.util.concurrent.*;

/**
 * This is daemon executor with all threads in the fixed pooz executed set as daemon.
 * Usually used for working with pooled-connections
 * @author Ganesh
 * @date 07-04-2015
 * @since 1.0-SNAPSHOT
 */
class GenericDaemonExecutors {

    private static final ExecutorService executor = Executors.newFixedThreadPool(10, new ThreadFactory() {
        @Override
        public Thread newThread(@SuppressWarnings("NullableProblems") Runnable r) {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            return t;
        }
    });

    public static Future submitFuture(Callable callable) {

        return executor.submit(callable);
    }
}
