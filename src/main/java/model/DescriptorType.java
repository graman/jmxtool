package model;

/**
 * @author Ganesh
 * @date 15-04-2015
 * @since 1.0-SNAPSHOT
 */
public enum DescriptorType {
    BeanDescriptor,
    ListDescriptor
}
